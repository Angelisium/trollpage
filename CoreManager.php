<?php
	namespace Core;

	class CoreManager {
		protected $ContentType = "text/html";
		protected $response = [
			'code' => 200,
			'desc' => 'OK'
		];

		public function __construct() {
			$this->_ENV = parse_ini_file(__ROOT__ . "/config.ini", true);
		}

		public function dispatch(...$args) {
			$RequestMethod = $_SERVER['REQUEST_METHOD'];
			$content = "";
			if(method_exists($this, $RequestMethod)) {
				$content = $this->$RequestMethod(...$args);
			} else {
				$this->response['code'] = 404;
				$this->response['desc'] = "Not Found";
				$content = $this->render("view/error-404");
			}

			header($_SERVER['SERVER_PROTOCOL'] . " " . implode(" ", $this->response));
			header("Content-type: {$this->ContentType}; charset=UTF-8");
			echo $content;
			die();
		}

		public function render($file_name, Array $_var = []):String {
			return $this->view($file_name, [
				'global' => $_var,
			]);
		}

		public function view($_file, Array $_var):String {
			ob_start();
			extract($_var);
			include("source/" . $_file . ".php");
			if(isset($extends)) {
				$_newvar = get_defined_vars();
				unset($_newvar["_file"]);
				unset($_newvar["_var"]);
				unset($_newvar["extends"]);
				return $this->view($extends, $_newvar);
			}
			return ob_get_clean();
		}

		public function PDO() {
			if(!isset($this->_pdo)) {
				try {
					$db_name    = $this->_ENV["DATABASE"]["DB_NAME"];
					$host       = $this->_ENV["DATABASE"]["HOST"];
					$username   = $this->_ENV["DATABASE"]["USER"];
					$password   = $this->_ENV["DATABASE"]["PASSWORD"];
					$this->_pdo = new \PDO(
						"mysql:dbname=" . $db_name . ";host=" . $host,
						$username,
						$password
					);
					$this->_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				} catch(\PDOException $e) {
					die( $e->getMessage() );
				}
			}
			return $this->_pdo;
		}
	}
