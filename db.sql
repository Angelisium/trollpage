-- Suppresion de la db

DROP TABLE `test`.`security`;
DROP TABLE `test`.`pages`;

-- Création de la db

CREATE TABLE `test`.`pages` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(90) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `test`.`security` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`page` INT NOT NULL,
	`ipv4` VARCHAR(15) NOT NULL,
	`date` VARCHAR(10) NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT FK_page_ip FOREIGN KEY (`page`) REFERENCES `pages`(`id`)
) ENGINE = InnoDB;

-- remplisage de la db

INSERT INTO `test`.`pages` (`name`) VALUES
	("Angelisium");