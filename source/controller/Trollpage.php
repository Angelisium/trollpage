<?php
	use Core\CoreManager;
	use source\small\library\Sql;

	class Controller extends CoreManager {
		public function GET(String $URI, String $ID):String {
			$PDO = $this->PDO();

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('*')
				->from('pages')
				->where('`id` = ?');
			$SQL_request = $PDO->prepare($SQL);
			$SQL_request->execute([$ID]);
			$troll = $SQL_request->fetch();
			$SQL_request->closeCursor();

			if(!$troll) {
				$this->response['code'] = 404;
				$this->response['desc'] = "Not Found";
				return $this->render("view/error-404");
			}

			$ipv4 = $_SERVER['REMOTE_ADDR'];
			$date = date('d-m-Y');

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('*')
				->from('security')
				->where(implode(' AND ', [
					'`page` = ?',
					'`ipv4` = ?',
					'`date` = ?',
				]));
			$SQL_request = $PDO->prepare($SQL);
			$SQL_request->execute([$ID, $ipv4, $date]);
			$isVisited = $SQL_request->fetch();
			$SQL_request->closeCursor();

			if(!$isVisited) {
				$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
				$SQL->insert('security')
					->column('page', 'ipv4', 'date')
					->values('?', '?', '?');
				$SQL_request = $PDO->prepare($SQL);
				$SQL_request->execute([$ID, $ipv4, $date]);
				$SQL_request->closeCursor();
			}

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('COUNT(*) AS troll_count')
				->from('security')
				->where('`page` = ?');
			$SQL_request = $PDO->prepare($SQL);
			$SQL_request->execute([$ID]);
			$troll_count = $SQL_request->fetch();
			$SQL_request->closeCursor();
			

			return $this->render('view/trollpage', [
				'troll_name' => $troll['name'],
				'troll_count' => $troll_count['troll_count'],
				'isVisited' => $isVisited ? true : false,
			]);
		}
	}
