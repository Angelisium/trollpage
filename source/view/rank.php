<?php

	$extends = "view/index"; 

	$title = " - ";

	$active = "rank";

	ob_start(null); ?>
				<h1>Classement</h1>
				<p>Voici les 10 plus grands trolleurs :</p>
				<table>
					<tbody>
						<tr>
							<th></th>
							<th>Pseudo</th>
							<th>Nombres de troll</th>
						</tr>
<?php
			$i = 0;
			foreach ($global['top_10'] as $top) {
				$i++; ?>
						<tr>
							<td><?= $i ?></td>
							<td><a href="/trollpage/<?= $top['id'] ?>"><?= $top['name'] ?></a></td>
							<td><?= $top['score'] ?></td>
						</tr>
<?php
			} ?>
					</tbody>
				</table>
<?php

	$content = ob_get_clean();
