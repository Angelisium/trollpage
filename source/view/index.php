<?php
	// design sauce : 
	// https://web.archive.org/web/20141006032514/http://trollpage.net/
	// https://web.archive.org/web/20141025110108/http://trollpage.net/37411
	// https://web.archive.org/web/20140302184946/http://trollpage.net/ranking
	// https://web.archive.org/web/20140116022937/http://trollpage.net:80/getlink

	$nav = [
		'home' => ["", '/', 'Accueil'],
		'rank' => ["", '/classement/', 'Classement'],
		'logn' => ["", '/connexion/', 'Obtenir mon lien'],
	];
	if(isset($active) && array_key_exists($active, $nav)) {
		$nav[$active][0] = ' class="highlight"';
	}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Troll Page - design</title>
		<link rel="stylesheet" href="/source/css/normalize.css">
		<link rel="stylesheet" href="/source/css/style.css">
	</head>
	<body>
		<header>
			<nav>
				<ul>
<?php
				foreach ($nav as $n) { ?>
					<li<?= $n[0] ?>><a href="<?= $n[1] ?>"><?= $n[2] ?></a></li>
<?php
				} ?>
				</ul>
			</nav>
		</header>
		<main>
<?= $content ?>
		</main>
		<footer>
			<p>Copyright 2022 - ANGELISIUM</p>
		</footer>
	</body>
</html>