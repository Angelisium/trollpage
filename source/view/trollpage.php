<?php

	$extends = "view/index"; 

	$title = " - ";

	$_img = ['01.png', '02.png', '03.png'];
	$_expression = [
		"Rholala !!",
		"Rho.. !",
		"Mais quels boulet !",
		"Nan mais ...",
	];

	ob_start(null);
		if(!$global['isVisited']) { ?>
				<h1>Vous avez été trollé !</h1>
<?php
		} else { ?>
				<p>
					<?= $_expression[array_rand($_expression, 1)] ?><br/>
					Vous vous êtes déjà fait troller par ce joueur aujourd'hui ...<br/>
					<small>Vous devriez vraiment essayer de <a href="/connexion/">vous venger</a> !</small>
				</p>
<?php
		} ?>
				<img src="/source/image/trollface/<?= $_img[array_rand($_img, 1)] ?>" alt="trollface">
				<p>
					<?= $global['troll_name'] ?> a déjà trollé <?= $global['troll_count'] ?> personnes.<br/>
					<small>Pour vous venger connectez-vous et essayez de le troller avec votre propre trollpages !</small>
				</p>
<?php

	$content = ob_get_clean();