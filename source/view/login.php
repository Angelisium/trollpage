<?php

	$extends = "view/index"; 

	$title = " - Connexion";

	$active = "logn";

	ob_start(null); ?>
				<h1>Inscription</h1>
				<p><b>Créez votre propre Troll Page !</b><br/><small>Et trollez tout le monde ...avec un petit lien!</small></p>
				<form method="POST">
					<input type="text" name="identifiant" placeholder="Mon pseudo" />
					<button>Créer</button>
				</form>
<?php
				if(isset($global['error'])) { ?>
					<p><?= $global['error'] ?></p>
<?php
				}
				if(isset($global['success'])) { ?>
					<p><?= $global['success'] ?><br/>
					Et voici votre lien : /trollpage/<?= $global['id'] ?></p>
<?php
				} ?>
<?php

	$content = ob_get_clean();