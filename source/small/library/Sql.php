<?php
	namespace source\small\library;
	# See : https://gitlab.com/-/snippets/2303172

	class Sql implements \Stringable {
		private $sql = [];

		public function __construct(
			private String $_prefix = ""
		) {}

		public function _escape(String $str):String {
			if(preg_match('/^`(.*)`$/', $str) < 1) {
				$str = "`" . $str . "`";
			}
			return $str;
		}

		public function _table(String $table):String {
			return $this->_escape($this->_prefix . $table);
		}

		public function _column(String $column):String {
			return $this->_escape($column);
		}

		public function _condition(String $condition):Self {
			$this->sql[] = $condition;
			return $this;
		}

		public function select(...$s):Self {
			$this->sql[] = "SELECT";
			$this->sql[] = implode(", ", $s);
			return $this;
		}

		public function insert(String $table):Self {
			$this->sql[] = "INSERT";
			$this->sql[] = "INTO";
			$this->sql[] = $this->_table($table);
			return $this;
		}
		public function update(String $table):Self {
			$this->sql[] = "UPDATE";
			$this->sql[] =  $this->_table($table);
			return $this;
		}
		public function delete(String $table):Self {
			$this->sql[] = "DELETE";
			return $this->from($table);
		}

		public function from(String $table):Self {
			$this->sql[] = "FROM";
			$this->sql[] =  $this->_table($table);
			return $this;
		}

		// INNER JOIN 
		// CROSS JOIN
		// LEFT JOIN
		// LEFT OUTER JOIN
		// RIGHT JOIN
		// RIGHT OUTER JOIN
		// FULL JOIN
		// FULL OUTER JOIN
		// SELF JOIN
		// NATURAL JOIN
		// UNION JOIN
		public function join(String $type, String $table):Self {
			$this->sql[] = strtoupper($type);
			$this->sql[] = "JOIN";
			$this->sql[] =  $this->_table($table);
			return $this;
		}

		public function on(String $condition):Self {
			$this->sql[] = "ON";
			return $this->_condition($condition);
		}
		
		public function where(String $condition):Self {
			$this->sql[] = "WHERE";
			return $this->_condition($condition);
		}

		public function having(String $condition):Self {
			return $this->_condition($condition);
		}

		public function group(String $column):Self {
			$this->sql[] = "GROUP";
			$this->sql[] = "BY";
			$this->sql[] = $this->_column($column);
			return $this;
		}

		// soon
		// public function union():Self { return $this; }
		// public function intersect():Self { return $this; }
		// public function except():Self { return $this; }

		public function order(Array $criteria):Self {
			$this->sql[] = "ORDER";
			$this->sql[] = "BY";
			$order = [];
			foreach ($criteria as $column => $o) {
				$order[] = $this->_column($column) . " $o";
			}
			$this->sql[] = implode(", ", $order);
			return $this;
		}
		public function limit(Int $count):Self {
			$this->sql[] = "LIMIT";
			$this->sql[] = $count;
			return $this;
		}

		public function offset($start):Self {
			$this->sql[] = "OFFSET";
			$this->sql[] = $start;
			return $this;
		}

		public function column(...$s):Self {
			$_col = [];
			foreach ($s as $column) {
				$_col[] = $this->_column($column);
			}
			$this->sql[] = "(" . implode(", ", $_col) . ")";
			return $this;
		}
		
		public function set(Array $values):Self {
			$this->sql[] = "SET";
			$_val = [];
			foreach ($values as $column => $val) {
				$_val[] = implode(" = ", [
					$this->_column($column),
					$val
				]);
			}
			$this->sql[] = implode(", ", $_val);
			return $this;
		}

		public function values(...$s):Self {
			$this->sql[] = "VALUES";
			$_val = [];
			foreach ($s as $value) {
				$_val[] = $value;
			}
			$this->sql[] = "(" . implode(", ", $_val) . ")";
			return $this;
		}

		public function __toString():String {
			return implode(" ", $this->sql);
		}
	}
