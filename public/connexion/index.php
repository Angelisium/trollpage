<?php
	define('__ROOT__', dirname(__DIR__, 2));
	require_once(__ROOT__ . "/AutoLoader.php");

	use Core\CoreManager;
	use source\small\library\Sql;

	class Controller extends CoreManager {
		public function GET():String {
			return $this->render('view/login');
		}

		public function POST():String {
			$PDO = $this->PDO();

			$troll_name = $_POST['identifiant'] ?: "Anonymous";
			$success = "$troll_name, vous avez déjà une trollpages !";

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('*')
				->from('pages')
				->where('`name` = ?');
			$GetTroll = $PDO->prepare($SQL);
			$GetTroll->execute([$troll_name]);
			$troll = $GetTroll->fetch();

			if(!$troll) {
				$success = "$troll_name, vous avez maintenant une trollpages !";

				$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
				$SQL->insert('pages')
					->column('name')
					->values('?');
				$SQL_request = $PDO->prepare($SQL);
				$SQL_request->execute([$troll_name]);
				$SQL_request->closeCursor();

				$GetTroll->execute([$troll_name]);
				$troll = $GetTroll->fetch();
			}

			$GetTroll->closeCursor();
			return $this->render('view/login', [
				'success' => $success,
				'id' => $troll['id'],
			]);
		}
	}

	$controller = new Controller();
	$controller->dispatch();