<?php
	define('__ROOT__', dirname(__DIR__));
	require_once(__ROOT__ . "/AutoLoader.php");

	use Core\CoreManager;
	use source\small\library\Sql;

	class Controller extends CoreManager {
		public function GET():String {
			$PDO = $this->PDO();

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('COUNT(*) AS TP_count')
				->from('pages');
			$SQL_request = $PDO->query($SQL);
			$TP_count = $SQL_request->fetch();
			$SQL_request->closeCursor();

			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);
			$SQL->select('COUNT(DISTINCT ipv4) AS T_count')
				->from('security');
			$SQL_request = $PDO->query($SQL);
			$T_count = $SQL_request->fetch();
			$SQL_request->closeCursor();

			return $this->render('view/home', [
				'TP_count' => $TP_count['TP_count'],
				'T_count'  => $T_count['T_count'],
			]);
		}
	}

	$controller = new Controller();
	$controller->dispatch();