<?php
	define('__ROOT__', dirname(__DIR__, 2));
	require_once(__ROOT__ . "/AutoLoader.php");

	use Core\CoreManager;
	use source\small\library\Sql;

	class Controller extends CoreManager {
		public function GET():String {
			$PDO = $this->PDO();
			$SQL = new Sql($this->_ENV['DATABASE']['TABLE_PREFIX']);

			$T_p = $SQL->_table('pages');
			$T_s = $SQL->_table('security');
			$SQL->select(
					"$T_p.`id`",
					"$T_p.`name`",
					"COUNT($T_s.`page`) AS `score`"
				)
				->from("pages")
				->join("LEFT", "security")
				->on("$T_p.`id` = $T_s.`page`")
				->group("$T_p.`id`")
				->order([
					"score" => "DESC",
					"$T_p.`name`" => "ASC",
				])
				->limit(10)
				->offset(0);

			$SQL_request = $PDO->query($SQL);
			$top_10 = $SQL_request->fetchAll();
			$SQL_request->closeCursor();
			return $this->render('view/rank', [
				'top_10' => $top_10,
			]);
		}
	}

	$controller = new Controller();
	$controller->dispatch();
